(in-package :twitter)

;;
;; Element record sugar
;;

;; Elements are structures that mirror the fields of twitter elements.
;; We allow specific types to maintain identity and effectively make 
;; objects immutable.  The macro records description information and
;; provides some convenience conversions of _ symbols to - symbols.

(defvar *element-descriptions* (make-hash-table :test #'eq)
  "Lookup element descriptions")

(defmacro define-element (name embedded &body arguments)
  "A macro for compactly defining a twitter element.  Args are lists of
   (slotname description value-type).  First two elts are optional unique-p
    and an optional description"
  (let* ((has-tag-p (symbolp (first arguments)))
	 (unique-p (when has-tag-p (first arguments)))
	 (description (if has-tag-p (second arguments) (first arguments)))
	 (args (if has-tag-p (cddr arguments) (cdr arguments))))
    `(progn
       (defstruct ,name
	 ,@(mapcar #'car args))
       (defun ,(parse-fn-name name) (rec)
	 (parse-element-record ',name rec ',embedded ,unique-p))
       (let ((args ',args))
	 (add-conversions args)
	 (record-arg-descriptions ',name ,description args)))))


(eval-when (:compile-toplevel :load-toplevel)
  (defun add-conversions (args)
    "Add twitter->lisp conversions for define-element args"
    (mapcar #'maybe-add-conversion (mapcar #'car args)))

  (defun record-arg-descriptions (type description args)
    "Record the descriptions for specific slot types.  This
   works because they're universal in the twitter API"
    (setf (gethash type *element-descriptions*) 
	  (list description (mapcar #'first args)))
    (loop for arg in args do
	 (let ((keyarg (first arg)))
	   (setf (gethash keyarg *element-descriptions*)
		 (rest arg)))))

  ;; Lookup default functions

  (defun parse-fn-name (type)
    "Given a type, return the canonical parser name.  Created automatically"
    (intern (concatenate 'string "PARSE-" (symbol-name type))
	    #.(find-package :twitter)))

  (defun make-fn-name (type)
    "Given a type, return the canonical structure make instance argument.
   Created automatically."
    (intern (concatenate 'string "MAKE-" (symbol-name type))
	    #.(find-package :twitter)))

  (defun lookup-fn-name (type)
    "Given a type, return the name lookup fn. User defined."
    (intern (concatenate 'string "LOOKUP-" (symbol-name type))
	    #.(find-package :twitter))))


;;
;; Parsing records
;;

(defgeneric parse-record (response prim-type)
  (:documentation "Default response parser for primitive types; assume target structure")
  (:method (response prim-type)
    (funcall (parse-fn-name prim-type) response))
  (:method (response (prim-type (eql :integer)))
    (parse-integer response))
  (:method (response (prim-type (eql :string)))
    response)
  (:method (response (prim-type (eql :identity)))
    response))

(defun parse-element-record (type rec embedded unique-p)
  "Generic parsing function"		     
  (let* ((lisprec (twitter->lisp-alist rec))
	 (existing (when (fboundp (lookup-fn-name type))
		     (funcall (lookup-fn-name type) lisprec))))
    (if existing
	(prog1 existing
	  (unless unique-p ;; maintains eq, but refreshes values
	    (update-element-fields existing lisprec)
	    (create-embedded-elements existing embedded)))
	(let ((new (default-make-element lisprec type)))
	  (create-embedded-elements new embedded)
	  (register-twitter-object new)
	  new))))

(defun default-make-element (rec type)
  "Make an element of type from record."
  (apply (make-fn-name type)
	 (alist->plist rec)))

(defun update-element-fields (element rec)
  "Since we don't know whether we are updating an existing or new element."
  (loop for (field . value) in rec do
        (setf (slot-value element (localize-symbol field #.(find-package :twitter))) value)))

(defun create-embedded-elements (elt embedded)
  "For any slot value that is designated embedded, take the alist
   and create an instance of that embedded object"
  (when (stringp (slot-value elt 'id))
    (setf (slot-value elt 'id)
	  (parse-integer (slot-value elt 'id))))
  (loop for (argname type) in embedded do
       (let ((value (slot-value elt argname)))
	 (when value
	   (setf (slot-value elt argname)
		 (parse-embedded-element type value))))))

(defun parse-embedded-element (type value)
  "Parse elements of type from value, map over the value
   as a list of type is of type consp"
  (when (consp value)
    (if (consp type)
	(mapcar (lambda (entry)
		  (funcall (parse-fn-name (first type)) entry))
		value)
	(funcall (parse-fn-name type) value))))
  
			
;;
;; Describe an element
;;

(defun get-description (type)
  (gethash type *element-descriptions*))

(defun element-help (type)
  (destructuring-bind (desc slots) (get-description type)
    (format t "~A~%~A~%~%Slots:~%~{~A: ~A~%~}~%"
	    type desc 
	    (loop for slot in slots 
		 nconc (list slot (car (get-description slot)))))))
  

;;
;; User elements
;;

(defvar *twitter-users* (make-hash-table :test #'equal)
  "A hash of previously seen users to avoid re-parsing")

(define-element twitter-user ((status tweet)) 
  "This is the record for a twitter user; it stores both basic 
     and extended info."
  (id "A permanent unique id referencing an object, such as user or status" nil)
  (name "" nil)
  (screen-name "" nil)
  (password "" nil)
  (location "" nil)
  (description "" nil)
  (profile-image-url "" nil)
  (url "" nil)
  (protected "" nil)
  (followers-count "" nil)
  ;; Embedded status
  (status "" nil)
  ;; Extended
  (profile-background-color "" nil)
  (profile-text-color "" nil)
  (profile-link-color "" nil)
  (profile-sidebar-color "" nil)
  (profile-sidebar-border-color "" nil)
  (profile-sidebar-fill-color "" nil)
  (friends-count "" nil)
  (created-at "" nil)
  (favourites-count "" nil)
  (utc-offset "" nil)
  (time-zone "" nil)
  (profile-background-image-url "" nil)
  (profile-background-tile "" nil)
  (following "" nil)
  (notifications "" nil)
  (statuses-count "" nil))

(defmethod print-object ((user twitter-user) stream)
  (format stream "#<TWITTER-USER '~A'>" (twitter-user-screen-name user)))

(defun get-user (ref)
  (if (twitter-user-p ref) ref
      (aif (gethash ref *twitter-users*) it
	   nil)))

(defun lookup-twitter-user (rec)
  (let ((name (get-value :screen-name rec)))
    (gethash name *twitter-users*)))

(defmethod register-twitter-object ((user twitter-user))
  (setf (gethash (twitter-user-screen-name user) *twitter-users*) user))

(defun user-http-auth (user)
  (when user
    (list (twitter-user-screen-name user)
	  (twitter-user-password user))))

(defmethod describe-object ((user twitter-user) stream)
  (format stream "Name: ~A ('~A') id:~A~%" 
	  (twitter-user-name user)
	  (twitter-user-screen-name user)
	  (twitter-user-id user))
  (format stream "Created at: ~A~%" (twitter-user-created-at user))
  (format stream "Description: ~A~%" (twitter-user-description user))
  (format stream "Counts: friends ~A, followers ~A, statuses ~A~%" 
	  (twitter-user-friends-count user)
	  (twitter-user-followers-count user)
	  (twitter-user-statuses-count user))
  (format stream "Location: ~A~%" (twitter-user-location user))
  (format stream "Time Zone: ~A~%" (twitter-user-time-zone user)))


;;
;; Status/Tweet
;;

(define-element tweet ((user twitter-user))
  "A status element consisting of information on a status and a nested
   user element"
  (id "" nil)
  (created-at "" nil)
  (text "" nil)
  (source "" nil)
  (truncated "" nil)
  (favorited "" nil)
  (in-reply-to-status-id "" nil)
  (in-reply-to-user-id "" nil)
  (in-reply-to-screen-name "" nil)
  ;; embedded user
  (user "" nil))

(defmethod print-object ((tweet tweet) stream)
  (format stream "#<TWEET '~A' id:~A>" 
	  (if (tweet-user tweet)
	      (twitter-user-screen-name (tweet-user tweet))
	      "none")
	  (tweet-id tweet)))

(defmethod describe-object ((tweet tweet) stream)
  (format stream "status: \"~A\"~%by: ~A (~A) on: ~A"
	  (tweet-text tweet)
	  (if (tweet-user tweet)
	      (twitter-user-name (tweet-user tweet))
	      "")
	  (if (tweet-user tweet)
	      (twitter-user-screen-name (tweet-user tweet))
	      "none")
	  (tweet-created-at tweet)))

(defmethod print-tweet (tweet)
  (format t "~A~%by ~A at ~A~%~%"
	  (tweet-text tweet)
	  (twitter-user-screen-name (tweet-user tweet))
	  (tweet-created-at tweet)))


(defun lookup-tweet (rec)
  (declare (ignore rec))
  nil)

(defmethod register-twitter-object ((tweet tweet))
  (declare (ignore tweet))
  nil)


;;
;; Messages
;;

(define-element twitter-message ((sender twitter-user) (recipient twitter-user))
  "This is a twitter message record"
  (id "" nil)
  (created-at "" nil)
  (text "" nil)
  (sender-id "" nil)
  (sender-screen-name "" nil)
  (recipient-id "" nil)
  (recipient-screen-name "" nil)
  (sender "" nil)
  (recipient "" nil))

(defmethod print-object ((msg twitter-message) stream)
  (format stream "#<message '~A' id:~A>" 
	  (twitter-message-sender-screen-name msg)
	  (twitter-message-id msg)))

(defmethod describe-object ((msg twitter-message) stream)
  (format stream "~A: ~A" 
	  (twitter-message-sender-screen-name msg)
	  (twitter-message-text msg)))

(defun lookup-message (rec)
  (declare (ignore rec))
  nil)

(defmethod register-twitter-object ((msg twitter-message))
  (declare (ignore msg))
  nil)

;;
;; Search results
;;

(define-element search-result ((results (search-ref)))
   "This is the results of a twitter search.  Metadata plus
    a list of search references."
   (id "" nil)
   (results "" nil)
   (since-id "" nil)
   (max-id "" nil)
   (warning "" nil)
   (refresh-url "" nil)
   (page "" nil)
   (results-per-page "" nil)
   (next-page "" nil)
   (completed-in "" nil)
   (query "" nil))


(defmethod print-object ((results search-result) stream)
  (format stream "#<TWITTER-SEARCH '~A'>" (search-result-query results)))

(defun lookup-search-result (rec)
  (declare (ignore rec)))

(defmethod register-twitter-object ((result search-result))
  (declare (ignore result)))

(defun search-results (result)
  (search-result-results result))

(defun print-search-results (result)
  (loop for ref in (search-results result) do
       (print-search-ref ref)))

   
(define-element search-ref ()
  "An individual search reference"
  (id "Integer ID of message" nil)
  (text "The text of the tweet" nil)
  (to-user "Screen name" nil)
  (to-user-id "ID of receiver" nil)
  (from-user "Screen name" nil)
  (from-user-id "ID of sender" nil)
  (source "Source of the result" nil)
  (created-at "The date of message creation" nil)
  (iso-language-code "Two character language code for content" nil)
  (profile-image-url "The profile image of the sender"))

(defmethod print-object ((ref search-ref) stream)
  (format stream "#<TWITTER-SEARCH-REF '~A'>" (search-ref-from-user ref)))

(defun lookup-search-ref (rec)
  (declare (ignore rec)))

(defun print-search-ref (ref)
  (format t "~A: ~A~%" 
	  (search-ref-from-user ref)
	  (search-ref-text ref)))

(defmethod register-twitter-object ((ref search-ref))
  (declare (ignore ref)))
