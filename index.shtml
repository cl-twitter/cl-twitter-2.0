<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
  <title><!--#include virtual="project-name" --></title>
  <link rel="stylesheet" type="text/css" href="style.css"/>
  <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1"/>
</head>

<body>
 <div class="header">
   <h1><!--#include virtual="project-name" --></h1>
 </div>

<h3>About</h3>

<p>This is a simple interface to the <a href="http://apiwiki.twitter.com/REST+API+Documentation">Twitter API</a> written by Ian Eslick.  It also implements the <a href="http://apiwiki.twitter.com/Search+API+Documentation">Search API</a> and provides a <a href="http://tinyurl.com/">tinyurl</a> mapping API and the REPL interface performs automatic conversions to tiny urls.  The library is currently only available via darcs.</p>

<code>darcs get http://www.common-lisp.net/project/cl-twitter/darcs/cl-twitter</code><br><br>

I will be putting a tarball up on cliki for asdf-install after it's had some more exercise.  There are at least two other libraries out there that came out literally on the same day as I posted this to cl.net.  <a href="http://chaitanyagupta.com/lisp/cl-twit/">'<code>cl-twit</code>'</a> by Chaitanya Gupta and <a href="http://seashellsandxml.co.uk/code/">'<code>cl-twitter</code></a>' by <a href="http://seashellsandxml.co.uk/">Chris Davies</a>.  With permission of the author, I reproduced most of the cl-twit REPL API in the latest patch as well as used his tinyurl function.

<h3>Loading cl-twitter</h3>

<p>cl-twitter depends on: cl-json, drakma, trivial-http, anaphora and cl-ppcre

<p><code>(asdf:oos 'asdf:load-op :cl-twitter)</code> -> Creates package <code>:twitter</code>
<h3>API for the REPL</h3>


<h3>Authentication</h3>

<p>Most API commands require an authenticating user.  An
authenticating user (with valid username & password slots) can be
passed to a command using the <code>:user</code> option.  There is a convenience
function authenticate-user which takes a username and password,
creates and populates a user object (if authentication works) and sets
a global variable <code>*twitter-user*</code> which will be used to
authenticate calls by default.  The <code>:user</code> option overrides the default
parameter.

<h3>User-level REPL API</h3>

<p>There is a nice set of interactive routines for use at the REPL.  Each command prints the return values directly, then returns a set of internal objects represneting the full details of each reply.  You can use 'describe' on any element to see key fields.

<pre>
(public-timeline)            ;; public timeline

;; User specific commands
(authenticate-user screen-name password) ;; will set the default user if valid
(timeline)            ;; timeline for current authenticated user or use :id keyword argument
(friends-timeline)    ;; full timeline including friends tweets
(send-tweet text)     ;; will post a tweet / status update for the current user
(update text)         ;; alias for send-tweet

;; Replies
(reply-to tweet text) ;; Send a reply
(@reply-to tweet text) ;; Send a reply, but prepend @username to text

;; Messages
(messages)            ;; Your messages
(send-message user message) ;; Send a message
(sent-messages)       ;; Messages you've sent
</pre>

<p>Example run:

<pre>
TWITTER&gt; (authenticate-user "username" "password")
#&lt;TWITTER-USER 'ieslick'&gt;

TWITTER&gt; (user-timeline)
status: "The lisp twitter api is nearly done!"
by: First Last (username) on: Tue Feb 17 00:49:11 +0000 2009

status: "Benefits of working from home; lunchtime stew in Cambridge, MA http://loopt.us/-Hps2Q"
by: First Last (username) on: Mon Feb 16 17:49:52 +0000 2009

status: "My lisp can tweet!"
by: First Last (username) on: Mon Feb 16 04:52:28 +0000 2009

status: "Family outing! in Cambridge, MA http://loopt.us/zUhMpQ"
by: First Last (username) on: Sun Feb 15 21:14:26 +0000 2009

(#&lt;TWEET 'username' id:1219635898&gt; #&lt;TWEET 'username' id:1217269236&gt;
 #&lt;TWEET 'username' id:1215984895&gt; #&lt;TWEET 'username' id:1214353382&gt;)

TWITTER&gt; (print-tweets *)
status: "The lisp twitter api is nearly done!"
by: First Last (username) on: Tue Feb 17 00:49:11 +0000 2009

status: "Benefits of working from home; lunchtime stew in Cambridge, MA http://loopt.us/-Hps2Q"
by: First Last (username) on: Mon Feb 16 17:49:52 +0000 2009

status: "My lisp can tweet!"
by: First Last (username) on: Mon Feb 16 04:52:28 +0000 2009

status: "Family outing! in Cambridge, MA http://loopt.us/zUhMpQ"
by: First Last (username) on: Sun Feb 15 21:14:26 +0000 2009

TWITTER&gt; (send-tweet "cl-twitter is released!")
#&lt;TWEET 'username' id:1219635898&gt;

TWITTER&gt; (describe *)
by: usernam (First Last) created: Tue Feb 17 17:33:33 +0000 2009
msg: cl-twitter is released!
; No value
</pre>

<H3>Search API</h3>

The search API returns a 'search-result element which contains a set
of 'search-ref elements accessible via (search-result-results elt)

<pre>
(do-search "query string" &rest args) ;; is a shortcut for
   (twitter-op :search :q "query string" &rest args)
   ;; and returns two values: the list of refs and the 'search-result elt.

(twitter-trends)  ;; returns the top 20 twitter search trends

;; Can also directly do tinyurl conversions
(get-tinyurl "url")
</pre>

Example use:

<pre>
TWITTER&gt; (twitter-search "#lisp")
(#&lt;TWITTER-SEARCH-REF 'kotrit'&gt; #&lt;TWITTER-SEARCH-REF 'gibsonf1'&gt;
 #&lt;TWITTER-SEARCH-REF 'dhess'&gt; #&lt;TWITTER-SEARCH-REF 'baphled'&gt;
 #&lt;TWITTER-SEARCH-REF 'TheSeth26'&gt; #&lt;TWITTER-SEARCH-REF 'cdeger'&gt;
 #&lt;TWITTER-SEARCH-REF 'taphon'&gt; #&lt;TWITTER-SEARCH-REF 'dhess'&gt;
 #&lt;TWITTER-SEARCH-REF 'dhess'&gt; #&lt;TWITTER-SEARCH-REF 'yonkeltron'&gt;
 #&lt;TWITTER-SEARCH-REF 'steveportigal'&gt; #&lt;TWITTER-SEARCH-REF 'duck1123'&gt;
 #&lt;TWITTER-SEARCH-REF 'edgargoncalves'&gt; #&lt;TWITTER-SEARCH-REF 'edgargoncalves'&gt;
 #&lt;TWITTER-SEARCH-REF 'adam_houston'&gt;)
#&lt;TWITTER-SEARCH '%23lisp'&gt;
TWITTER&gt; (mapcar #'search-ref-text *)
 ("Luckily Slime takes care of most of the formatting quite nicely - RT @dhess Riastradh's Lisp style rules #lisp http://tinyurl.com/2onk5x"
 "Riastradh's Lisp style rules #lisp http://tinyurl.com/2onk5x"
 "I really need to learn #LISP one day" "Why macros over functions?? #lisp"
 "Ich will Clojure fr .NET! Dynamisch, funktional, STM eingebaut, schnell: http://bit.ly/4qX28T #lisp #jvm"
 "What's a good ide for #Lisp, any good books on the language written recently?"
 "Technical issues re: Lisp-1 vs. Lisp-2 #pl #lisp http://tinyurl.com/2kh96k"
 "What Common Lisp conditions (exceptions) are really about #lisp http://tinyurl.com/8r5yeh"
 "@theseth26 the folks in #emacs on freenode are often less snippy than the folks on #lisp. i lurk there, will talk. hack strong and prosper!"
 "@joshdamon I'm tempted to call it twethival #lisp"
 "@horseman !stumpwm is the window manager for #emacs and #screen users. It's all keyboard commands, and written in common #lisp"
 "Drawing a tree graph of my objects and their slots, in #lisp. looking for the easier way to do it. feel free to drop tips and hints."
 "Finished a functional webapp server with offline work support. Serialized proxies fly over http between two #lisp servers, works well."
 "Too bad we are moving to an integrated system though. Really wanted to rewrite the whole thing in #Lisp one of these days. Or maybe #Python.")
</pre>

<h2>Error Handling</h2>

<p>Any API call errors throw a <code>twitter-api-condition</code>
which provides the return code and short and long code messages.  It
also provides the failing URI and the specific server message.  The
accessors are not exported to avoid conflicts, but the slotnames are:
<br> &nbsp;&nbsp; <code>return-code, short, long, request, uri.</code>

<p>You can play with this, for example, by trying to perform an API
command with invalid user authentication.  For debugging purposes the
API keeps the last condition in <code>twitter::*last-condition*</code>.

<h3>Low Level, Programmatic Interface and Objects</h3>

<p>The generic, low-level interface to the API is via the twitter-op function:<br><br>

&nbsp;&nbsp; <code>(twitter-op :command &rest args)</code>

<p>This top level function sends an API call for :command with any
arguments to the twitter server and returns one or more 'elements'
(see types below).  This API uses struct objects to encapsulate
composite values such as users or tweets.  I tried to be smart about
converting between names such as 'user_id' and the more natural
:user-id parameter names on the lisp side.

<pre>

TWITTER&gt; (twitter-op :user-show :id "lisp4life")
#&lt;TWITTER-USER 'lisp4life'&gt;

TWITTER&gt; (describe *)
Name: Lisp ('lisp4life') id:19387004
Created at: Fri Jan 23 08:47:36 +0000 2009
Description: &quot;I'm the Greatest Gay Rapper in the World!&quot;
Counts: friends 2, followers 0, statuses 1
Location: West Hollywood, CA
Time Zone: Alaska

TWITTER&gt; (twitter-op :friend-create :id "twitterapi" :follow t)
#&lt;TWITTER_USER 'twitterapi'
</pre>

<p>You can view documentation for commands and elements using:
<pre>
(command-help [command-name]) ;; prints a summary of all commands or command-name
(element-help 'type)
</pre>
valid types: twitter-user, tweet, twitter-message, search-result, search-ref

<h2>Mailing Lists</h2>

 <ul>
  <li>
   <a href="http://www.common-lisp.net/mailman/listinfo/cl-twitter-devel">
    cl-twitter-devel</a><br>for developers</li>
  <li>
   <a href="http://www.common-lisp.net/mailman/listinfo/cl-twitter-announce">
    cl-twitter-announce</a><br>for announcements.</li>

 </ul>

<h2>API Status</h2>

<p>This interface was interactively tested, but bugs remain and not all
features have been exercised.  Help tracking down the last of the 
bugs is always appreciated!

<p>I have also not completely documented all the element slots.  This
should be easy to fix and you can also refer to the twitter APIs.

<p>TODO:<br>
<ul>

<li> Verify all API commands (<b>important</b>)
<li> Add documentation from Web API spec to element slots (<b>important</b>)
<li> Add logging support to capture API command history <br>&nbsp;&nbsp;(simple callback fn with twitter-op command and optionally, send and post-json receive data)
<li> Add optional unique-ification to tweets and messages as we do with twitter-user
<li> Cookie auth. support? (low priority)
<li> OpenID?
</ul>

 <p>Back to <a href="http://common-lisp.net/">Common-lisp.net</a>.</p>

 <div class="check">
   <a href="http://validator.w3.org/check/referer">Valid XHTML 1.0 Strict</a>
 </div>
</body>
</html>
